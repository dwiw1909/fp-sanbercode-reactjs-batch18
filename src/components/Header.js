import React, {useContext} from 'react'
import { Layout, Menu, Image } from 'antd';
import { Link, useHistory } from 'react-router-dom'
import { UserOutlined } from '@ant-design/icons';
import logo from '../logo.svg'

import {Context} from '../store'

const { Header } = Layout;
const { SubMenu } = Menu

const HeaderComp = () => {
  const [user, setUser] = useContext(Context)
  const history = useHistory()

  const handleLogout = () => {
    setUser({})
    history.push('/')
    localStorage.removeItem('user')
  }
  return (
    <Header>
      <Link to='/' >
        <div className="logo">
          <Image className='logo-app' width={40} src={logo} />
          <Image className='logo-app' width={40} src={logo} />
          <Image className='logo-app' width={40} src={logo} />
        </div>
      </Link>
      <Menu theme="dark" mode='horizontal' style={{textAlign: 'right'}}>
        <Menu.Item key="1"><Link to="/">Home</Link></Menu.Item>

        <Menu.Item key="2"><Link to="/games">Games</Link></Menu.Item>
        <Menu.Item key="3"><Link to="/movies">Movies</Link></Menu.Item>
        {
          user.name
          ? <>
              <SubMenu key="sub1" icon={<UserOutlined />} title='Manage'>
                <Menu.Item key="11"><Link to="/games/manage">Manage Games</Link></Menu.Item>
                <Menu.Item key="12"><Link to="/movies/manage">Manage Movies</Link></Menu.Item>
              </SubMenu>
              <SubMenu key="sub2" icon={<UserOutlined />} title={`hi, ${user.name}`}>
                <Menu.Item key="21"><Link to="/user/editpassword">Edit Password</Link></Menu.Item>
                <Menu.Item key="22" onClick={handleLogout}>Logout</Menu.Item>
              </SubMenu>
            </>
          : <Menu.Item key="5"><Link to="/login">Login</Link></Menu.Item>
            // {/* <Menu.Item key="5"><Link to="/register">Register</Link></Menu.Item></> */}
        }
        

        
      </Menu>
    </Header>
  )
}

export default HeaderComp
