import React, {useContext, useEffect} from 'react'
import { Carousel, Image } from 'antd';
import {Context} from '../store'
import {Row} from 'antd'
import GamesCard from '../components/GamesCard'
import MoviesCard from '../components/MoviesCard';

const contentStyle = {
  height: '300px',
  color: 'white',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};

const Home = () => {
  const [,,games,,movies,,action] = useContext(Context)

  const dataCorousel = [
    {url: 'https://cg1.cgsociety.org/uploads/images/original/rights0reserved1-fnaf-the-savage-chil-1-61899199-ekwc.jpeg'},
    {url: 'https://scontent-lga3-2.cdninstagram.com/v/t51.2885-15/e35/104238221_628093627800629_1583291899152755379_n.jpg?_nc_ht=scontent-lga3-2.cdninstagram.com&_nc_cat=105&_nc_ohc=ezsrYCt5I2kAX986O_j&_nc_tp=18&oh=65e5b7da5dae12307a1a7bbd44bccb69&oe=5FB9C5D0'},
    {url: 'https://c.wallhere.com/photos/61/57/ultrawide_ultra_wide_video_games_Video_Game_Art_Monster_Hunter_World-1447755.jpg!d'},
  ]

  useEffect(() => {
    action.fetchGames()
    action.fetchMovies()
  },[])


  return (
    <div>
      <Carousel autoplay>
        {
          dataCorousel.map((val, i) => {
            return <div key={i}><h3 style={contentStyle}><Image style={{objectFit: 'cover'}} width='100%' src={val.url} /></h3></div>
          })
        }
      </Carousel>
      <br />
      <h1>Latest Game Post</h1>
      <Row>
        {
          games &&
          games.slice(games.length-4, games.length).map((val, i) => {
            return (<GamesCard key={i} data={val} />)
          })
        }

      </Row>
      <h1>Latest Movie Post</h1>
      <Row>
        {
          movies &&
          movies.slice(movies.length-4, movies.length).map((val, i) => {
            return (<MoviesCard key={i} data={val} />)
          })
        }

      </Row>

    </div>
  )
}

export default Home