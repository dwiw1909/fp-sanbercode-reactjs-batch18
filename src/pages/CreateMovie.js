import React, {useContext, useEffect, useState} from 'react'
import { Form, Input, Button, Select, InputNumber, Slider, message } from 'antd';
import {Context} from '../store'
import Axios from 'axios'
import { useParams, useHistory } from 'react-router-dom';

const { Option } = Select

const layout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 10,
  },
};
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not validate email!',
    number: '${label} is not a validate number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};

const CreateMovie = () => {
  const [user, , , , , , action] = useContext(Context)
  const [edit, setEdit] = useState(false)
  const [form] = Form.useForm();
  let {id} = useParams()
  let history = useHistory()

  useEffect(() => {
    if(id){
      setEdit(true)
      fetchDataEdit(id)
    }
  }, [id])

  const fetchDataEdit = (ID_MOVIE) => {
    Axios.get(`https://backendexample.sanbersy.com/api/data-movie/${ID_MOVIE}`)
    .then(({data}) => {
      let dataEdit = {
        year: data.year,
        title: data.title,
        image_url: data.image_url,
        duration: data.duration,
        genre: data.genre.split(', '),
        description: data.description,
        rating: data.rating,
        review: data.review
      }
      form.setFieldsValue(dataEdit)
    }).catch((err) => {
      console.log(err.response);
    });
  }

  const saveMovie = (data) => {
    Axios.post(`https://backendexample.sanbersy.com/api/data-movie`,
      data,
      {
        headers: {
          Authorization: `Bearer ${user.token}`
        }
      }
    )
    .then(() => {
      message.success('Add Movie Success')
      form.resetFields()
      action.fetchMovies()
      history.push('/movies/manage')
    }).catch((err) => {
      message.error(JSON.stringify(err.response.data))
      if(err.response.data.status === 'Token is Expired'){
        message.error('Login Expired, please login again')
        action.userLogout()
        history.push('/login')
      }
    });
  }

  const updateMovie = (data) => {
    Axios.put(`https://backendexample.sanbersy.com/api/data-movie/${id}`,
      data,
      {
        headers: {
          Authorization: `Bearer ${user.token}`
        }
      }
    )
    .then((res) => {
      message.success('Update Movie Success')
      form.resetFields()
      setEdit(false)
      action.fetchMovies()
      history.push('/movies/manage')
    }).catch((err) => {
      message.error(JSON.stringify(err.response.data))
      if(err.response.data.status === 'Token is Expired'){
        message.error('Login Expired, please login again')
        action.userLogout()
        history.push('/login')
      }
    });
  }
  
  const onFinish = (val) => {
    let data = {
      title: val.title,
      image_url: val.image_url,
      duration: val.duration,
      description: val.description,
      genre: val.genre.join(', '),
      rating: val.rating,
      year: val.year,
      review: val.review
    }
    if(edit){
      updateMovie(data)
    } else {
      saveMovie(data)
    }
  };

  return (
    <>
      {
        edit
        ? <h1 style={{textAlign: 'center'}}>Edit Movie</h1>
        : <h1 style={{textAlign: 'center'}}>Create New Movie</h1>
      }
      
      <Form {...layout} form={form} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages} initialValues={{duration: 120, year: 2020, rating: 5}}>
        <Form.Item
          name='title'
          label="Movie Title"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="genre"
          label="Genre"
          rules={[
            {
              required: true,
              message: 'Select movie genre',
              type: 'array',
            },
          ]}
        >
          <Select mode="multiple" placeholder="Select movie genre">
            <Option value="Action">Action</Option>
            <Option value="Adventure">Adventure</Option>
            <Option value="Animation">Animation</Option>
            <Option value="Comedy">Comedy</Option>
            <Option value="Crime">Crime</Option>
            <Option value="Documentary">Documentary</Option>
            <Option value="Drama">Drama</Option>
            <Option value="Family">Family</Option>
            <Option value="Fantasy">Fantasy</Option>
            <Option value="History">History</Option>
            <Option value="Horor">Horor</Option>
            <Option value="Music">Music</Option>
            <Option value="Mistery">Mistery</Option>
            <Option value="Romance">Romance</Option>
            <Option value="War">War</Option>
          </Select>
        </Form.Item>

        <Form.Item
          name="rating"
          label="Rating"
          rules={[
            {
              required: true,
              type: 'number'
            }
          ]}
        >
          <Slider
            max={10}
            marks={{
              0: '0',
              1: '1',
              2: '2',
              3: '3',
              4: '4',
              5: '5',
              6: '6',
              7: '7',
              8: '8',
              9: '9',
              10: '10',
            }}
          />
        </Form.Item>

        <Form.Item label="Duration (minutes)" rules={[
          {
            required: true,
            type: 'number'
          },
        ]}>
          <Form.Item name="duration" noStyle>
            <InputNumber max={5000} min={1} type='number'   />
          </Form.Item>
        </Form.Item>
        
        <Form.Item label="Release Year" rules={[
          {
            required: true,
            type: 'number'
          },
        ]}>
          <Form.Item name="year" noStyle>
            <InputNumber max={2020} min={1945} type='number'   />
          </Form.Item>
        </Form.Item>

        <Form.Item
          name='description'
          label="Description"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item
          name='review'
          label="Review"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item
          name='image_url'
          label="Image URL"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 11 }}>
          <Button type="primary" htmlType="submit">
            {
              edit
              ? 'Edit'
              : 'Create'
            }
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default CreateMovie