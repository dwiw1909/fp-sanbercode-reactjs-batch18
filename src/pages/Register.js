import React from 'react'
import Axios from 'axios'
import { Form, Input, Button, message } from 'antd';
import {Link, useHistory} from 'react-router-dom'

const layout = {
  labelCol: { span: 10 },
  wrapperCol: { span: 5 },
};

const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not validate email!',
    number: '${label} is not a validate number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};

const Register = () => {
  const [form] = Form.useForm();
  const history = useHistory()

  const onFinish = val => {
    Axios.post(`https://backendexample.sanbersy.com/api/register`, {
      name: val.user.name, email: val.user.email, password: val.password
    }, { headers: {'Access-Control-Allow-Origin': true}})
    .then(() => {
      form.resetFields()
      message.success('Register Success :)')
      history.push('/login')
    }).catch(({response}) => {
      message.error(JSON.stringify(response.data));
    });
  }
  return (
    <div style={{marginTop: '20px'}}>
      <h1 style={{textAlign: 'center', marginBottom: '20px'}}>Register</h1>
    
      <Form {...layout} form={form} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
        <Form.Item name={['user', 'name']} label="Name" rules={[
          { required: true },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (!value || value.length >= 3) {
                return Promise.resolve();
              }
              return Promise.reject('Name min 3 characters!');
            },
          }),
        ]}>
          <Input />
        </Form.Item>

        <Form.Item
          name={['user', 'email']}
          label="Email"
          rules={[
            { 
              required: true,
              message: 'Please input your Email corectly!',
              type: 'email' }
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="password"
          label="Password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || value.length >= 6) {
                  return Promise.resolve();
                }
                return Promise.reject('Password min 6 characters!');
              },
            }),
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="confirm"
          label="Confirm Password"
          dependencies={['password']}
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please confirm your password!',
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(`Password didn't match!`);
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        
        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 11 }}>
          <Button type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>

        <p style={{textAlign: 'center'}}>Already have an account? <Link to='/login'>Log in</Link></p>
      </Form>
    </div>
  );
}

export default Register
